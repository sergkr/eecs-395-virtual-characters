(cd-here)

(require 'state-machines)

(define-twig-object doctor Adult @(0 0 0) @(0 0 1)
  Color: Color.White)

(define-component input TextInputHUD
  Prompt: "> "
  Font: game.DialogFont
  ForceLowerCase: true)

(within doctor
  (load "doctor.scm")
  (consult "eliza.pl")
  (consult "doctor.pl"))