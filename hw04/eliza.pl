%%%
%%% Simple ELIZA-style rule matcher
%%% Assumes a database of rules of the form
%%%   rule(Priority, Pattern, Response, Action)
%%% where 
%%% Priority is how badly this rule wants to run if matched (1=high priority)
%%% Pattern a list of words (atoms) and wildcards of the form:
%%%   word(X)      - matches X to one word of Utterance
%%%   phrase(X)    - matches X to a phrase of Utterance
%%%   word(X, P)   - matches X to one word of Utternace for which P(X) is true
%%%   phrase(X, P) - matches X to a phrase of Utterance for which P(X) is true
%%% Response is the wordlist constructed using variables from the pattern
%%% Action is a Prolog goal that gets called if this rule is chosen
%%%

% This is called by the character to respond to user input
% Convert to/from string form and call response/2
response_text(UtteranceString, ReplyString) :-
	word_list(UtteranceString, UtteranceWords),
	response(UtteranceWords, ReplyWords),
	word_list(ReplyString, ReplyWords).

% Called with word lists rather than strings
response(Utterance, Reply) :-
        flatmap(canonicalize, Utterance, Expanded),
        best_match(Expanded, Reply1, Action),
	Action,
        flatmap(switch_viewpoint_in_list, Reply1, Reply).

% The best match for Utterance is Reply/Action
best_match(Utterance, Reply, Action) :-
      	arg_min([Reply, Action], Priority,
		randomize(match_rule(Priority, Utterance, Reply, Action))).

% Uterance matches some rule with the given Reply, Action, and Priority
match_rule(Priority, Utterance, Reply, Action) :-
	rule(Priority, In, Reply, Action), match(In, Utterance).

:- randomizable(rule).

% match(Pattern, Utterance)
match([], []).
match([phrase(X) | PTail], Utterance) :-
	append(X, Y, Utterance), match(PTail, Y).
match([phrase(X, P) | PTail], Utterance) :-
	append(X, Y, Utterance), match(PTail, Y), P.
match([word(Word) | PTail], [Word | UTail]) :-
	match(PTail, UTail).
match([word(Word, P) | PTail], [Word | UTail]) :-
	P, match(PTail, UTail).
match([Word | PTail], [Word | UTail]) :-
	atomic(Word), match(PTail, UTail).

%%
%% Viewpoint switching
%% Substitutes me for you, etc.
%%

% switch_viewpoint_in_list(X, Flipped)
% Flips words in X if X is a list, otherwise leaves it alone.
switch_viewpoint_in_list(X, X) :- atomic(X).
switch_viewpoint_in_list(X, Y) :- list(X), maplist(switch_viewpoint, X, Y).

% Switch viewpoint of n individual word
switch_viewpoint(X, Y) :- viewpoint_pair(X, Y), !.
switch_viewpoint(X, Y) :- viewpoint_pair(Y, X), !.
switch_viewpoint(X, X).

% Pairs of words to swap when switching viewpoint.
viewpoint_pair(i, you).
viewpoint_pair(me, you).
viewpoint_pair(my, your).
viewpoint_pair(myself, yourself).
viewpoint_pair(mine, your).
viewpoint_pair(are, am).

%%
%% Canonicalization of input
%% Expand canonical_forms, substitute synonyms, etc.
%%

canonicalize(X, Y):- canonical_form(X, Y), !.
canonicalize(X, X).

canonical_form('i\'ve', [i, have]).
canonical_form('i\'ll', [i, will]).
canonical_form('i\'m', [i, am]).
canonical_form('you\'ve', [you, have]).
canonical_form('you\'ll', [you, will]).
canonical_form('you\'re', [you, are]).
canonical_form('can\'t', [can, not]).
canonical_form('won\'t', [will, not]).

%%
%% Utilities
%%

% map P over L1 and flatten it to make L2.
flatmap(P, L1, L2) :-
	maplist(P, L1, T), flatten(T, L2).

% Need this or the do_nothing actions in the rules will fail.
do_nothing.