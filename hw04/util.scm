;; This file contains some useful utility procedures and gestures that
;; could be reused from one assignment to the next.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Generic utility methods ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; Returns a unit vector pointing directly to the right of
;; the given character's facing direction
(define-signal-procedure (right-side-of character)
	(unit (cross character.FacingDirection
							 (vector 0 1 0))))

;; Returns a unit vector pointing directly to the left of
;; the given character's facing direction
(define-signal-procedure (left-side-of character)
	(- (right-side-of character)))


;; Turns the specified character in the given direction (vector).
(define-signal-procedure (orient-to character desired-direction)  
	(let ((error (* 100
									(dot character.FacingDirection
											 (cross desired-direction
															@(0 1 0))))))
		(posture-force  pelvis-yaw: error
										shoulder-yaw: error)))



;;;;;;;;;;;;;;;;;;;;;;;
;; Reusable gestures ;;
;;;;;;;;;;;;;;;;;;;;;;;


;; Raise the arm to the specified height.
(define-signal-procedure (raise-arm arm height)
	(limb-command end-acceleration:
								(* (- (+ arm.Root.Position
												 (vector 0 height 0))
											arm.End.Position)
									 400)))

;; Move the hand to the specified position.
(define-signal-procedure (move-hand-to arm position)
	(limb-command end-acceleration:
								(* (- position arm.End.Position)
									 200)))

;; Move the elbow to the specified position
(define-signal-procedure (move-elbow-to arm position)
	(limb-command joint-acceleration:
								(* (- position arm.Joint.Position)
									 200)))

;; Makes the character slap a given position.
;;   arm: Which arm to slap with
;;   target-position: Position in space that gets slapped.
;; Usage example:
;;   (define-right-arm-behavior slap-opposing-player
;;	   (slap this.Arms.Right victim.Head.Midpoint)
;;		 activation: (if (in-state? attack-opposing-team-state-machine 'slap-victim) 1 0)))
(define-signal-procedure (slap arm target-position)
	(move-hand-to arm (+ arm.Root.Position
											 (- target-position	(- (vector 0 1.5 0)
																						 (* (vector 4 0 0)
																								(if (> (sin (* 10 time)) 0)
																										-1
																										1)))))))

;; Shrug gesture
;; -------------
;;
;; Example usage (where the character is called 'doctor'):
;; 
;; (define-posture-behavior shrug-shoulders-behavior
;; 	 (shrug-shoulders)
;; 	 activation: (if (key-down? Keys.A) 1 0))
;; 	
;; (define-left-arm-behavior shrug-left-arm-behavior
;; 	 (shrug-left-arm doctor)
;; 	 activation: (if (key-down? Keys.A) 1 0))
;; 	
;; (define-right-arm-behavior shrug-right-arm-behavior
;; 	 (shrug-right-arm doctor)
;; 	 activation: (if (key-down? Keys.A) 1 0))

(define-signal-procedure (shrug-shoulders)
	(posture-force shoulder-force: @(0 200 0)))

(define-signal-procedure (shrug-left-arm character)
	(limb-command
	 joint-acceleration: (* 50 (- (left-side-of character)
																@(0 2 0)))))

(define-signal-procedure (shrug-right-arm character)
	(limb-command
	 joint-acceleration: (* 50 (- (right-side-of character)
																@(0 2 0)))))


;;;;;;;;;;;;;
;; Dancing ;;
;;;;;;;;;;;;;

(define-signal beat (beat-tracker true 120))
(define-signal first-measure? (< beat.TwoBar.Phase 0.5))
(define-signal first-beat? (= beat.Measure.BeatNumber 0))

;; Example on how to use these procedures to implement dancing:
;;
;; (define-posture-behavior swing-left-right   
;; 	(orient-to doctor (vector (if first-measure? 1 -1)
;; 														0
;; 														1))
;; 	stop-trigger: (if (in-state? dialog 'stopping) true false))
;;
;; (define-left-arm-behavior reach-left 
;; 	(raise-arm doctor.Arms.Left
;; 						 (* (if  (and first-measure? first-beat?)
;; 										 1 -1)
;; 								doctor.Arms.Left.Length))
;; 	stop-trigger: (if (in-state? dialog 'stopping) true false))
;;
;; (define-right-arm-behavior reach-right 
;; 	(raise-arm doctor.Arms.right
;; 	           (* (if  (and  (not first-measure?)  first-beat?)
;; 										 1 -1)
;; 								doctor.Arms.Right.Length))
;; 	stop-trigger: (if (in-state? dialog 'stopping) true false))
;;
;; (define-posture-behavior bump
;; 	(posture-force pelvis-force:
;; 								 (* (* (if (< beat.Beat.Phase 0.2) 100 0)
;; 											 (if first-measure? 1 -1))
;; 										doctor.pelvisRight))
;; 	stop-trigger: (if (in-state? dialog 'stopping) true false))

