(load "util.scm")

(define (send-game-state args ...)
  (game.SendMessage (new GameStateMessage null args)))

(define-signal username
	(register 'username "Nathan"))

(define-state-machine dialog
  (welcome1 (enter (set-timeout 2))
						(messages (TimeoutMessage (this.Say "Welcome to Microsoft Fred")
																			(goto welcome2))))
	(welcome2 (enter (set-timeout 2))
						(messages (TimeoutMessage (this.Say "Microsoft's revolutionary new take on the operating system.")
																			(goto welcome3))))
	(welcome3 (enter (set-timeout 3))
						(messages (TimeoutMessage (this.Say "I am Fred, your humble guide and servant.")
																			(goto welcome4))))
  (welcome4 (enter (set-timeout 3))
						(messages (TimeoutMessage (this.Say "We've done away with files, applications, taskbars, and start menus...")
																			(goto welcome5))))
	(welcome5 (enter (set-timeout 3))
						(messages (TimeoutMessage (this.Say "In Microsoft Fred, you just say what you want to do!")
																			(goto welcome6))))
	(welcome6 (enter (set-timeout 2))
						(messages (TimeoutMessage (this.Say "So let's get started.")
																			(goto ask-username))))
	(ask-username (enter (set-timeout 2))
								(messages (TimeoutMessage (this.Say "Before we begin, could you please type your name?"))
													(TextInputMessage
													 (set-register! username $message.Text)
													 (this.Say (String.Format "Hello {0}" username.Value))
													 (goto accept-input))))
  (accept-input (enter (set-timeout 1))
								(messages (TimeoutMessage (this.Say (String.Format "What would you like to do, {0}?" username.Value)))
													(TextInputMessage
													 (with-solution (response_text ,$message.Text X)
															 (unless (null? X)
																 (this.Say (X.ToString))))
													 (goto actioning))))
  (actioning (enter (set-timeout 3))
             (messages (TimeoutMessage
												(if (and (in-state? browse-internet-state-machine 'inactive)
																 (in-state? shutdown-state-machine 'inactive))
														(goto stopping)))))
  (stopping (enter (set-timeout 1))
            (messages (TimeoutMessage
                       (goto accept-input)
                       (click-annoyance.SetState "start"))))
  (done (enter (set-timeout 1))
        (messages (TextInputMessage
                   (this.Say "I can't deal with this."))
                  (TimeoutMessage
                   (this.Say "I can't deal with this.")
                   (set-timeout 1)))))

(define-state-machine click-annoyance
  (start (messages ((click-left-on? doctor)
                    (doctor.Say "Please interact with me via the prompt.")
                    (goto stage1))))
  (stage1 (messages ((click-left-on? doctor)
                     (doctor.Say "I am not able to process pointer-based input.")
                     (goto stage2))))
  (stage2 (messages ((click-left-on? doctor)
                     (doctor.Say "Seriously, stop clicking on me. It won't get anything done.")
                     (goto stage3))))
  (stage3 (messages ((click-left-on? doctor)
                     (doctor.Say "For the LAST time, STOP clicking on me!")
                     (goto stage4))))
  (stage4 (messages ((click-left-on? doctor)
                     (doctor.Say "I can't deal with this.")
                     (goto done)
                     (dialog.SetState "done"))))
  (done (messages ((click-left-on? doctor)
                   (doctor.Say "I can't deal with this.")))))

(define-locomotion-behavior run-away
  (vector 0 0 -100)
  start-trigger: (if (in-state? click-annoyance 'done) true false)
  call-activation: 100)

;; Raise right hand gesture
;; ------------------------
(define-right-arm-behavior raise-right-hand
  (move-hand-to doctor.Arms.Right
                (+ doctor.Arms.Right.Root.Position
                   (vector 0 2 0)))
  stop-trigger: (if (in-state? dialog 'stopping) true false))

;; Shrug gesture
;; -------------

(define-posture-behavior shrug-shoulders-behavior
  (shrug-shoulders)
  stop-trigger: (if (in-state? dialog 'stopping) true false))

(define-left-arm-behavior shrug-left-arm-behavior
  (shrug-left-arm doctor)
  stop-trigger: (if (in-state? dialog 'stopping) true false))

(define-right-arm-behavior shrug-right-arm-behavior
	(shrug-right-arm doctor)
	stop-trigger: (if (in-state? dialog 'stopping) true false))

;; Dancing
;; -------

(define-posture-behavior swing-left-right   
	(orient-to doctor (vector (if first-measure? 1 -1)
														0
														1))
	stop-trigger: (if (in-state? dialog 'stopping) true false))

(define-left-arm-behavior reach-left 
	(raise-arm doctor.Arms.Left
						 (* (if  (and first-measure? first-beat?)
										 1 -1)
								doctor.Arms.Left.Length))
	stop-trigger: (if (in-state? dialog 'stopping) true false))

(define-right-arm-behavior reach-right 
	(raise-arm doctor.Arms.right
	           (* (if  (and  (not first-measure?)  first-beat?)
										 1 -1)
								doctor.Arms.Right.Length))
	stop-trigger: (if (in-state? dialog 'stopping) true false))

(define-posture-behavior bump
	(posture-force pelvis-force:
								 (* (* (if (< beat.Beat.Phase 0.2) 100 0)
											 (if first-measure? 1 -1))
										doctor.pelvisRight))
	stop-trigger: (if (in-state? dialog 'stopping) true false))


;; Browse the Internet
;; -------------------

(define-state-machine browse-internet-state-machine
	(inactive (enter (set-timeout 0)))
	(start (enter (set-timeout 2))
				 (messages (TimeoutMessage (this.Say "Hmm, there appears to be a connection problem.")
																	 (goto state2))))
	(state2 (enter (set-timeout 2))
					(messages (TimeoutMessage (this.Say (String.Format "Hold on a second, {0}, let me attempt to diagnose the problem." username.Value))
																		(goto state3))))
	(state3 (enter (set-timeout 2))
					(messages (TimeoutMessage (this.Say "Starting network diagnostics...")
																		(goto state4))))
	(state4 (enter (set-timeout 2))
					(messages (TimeoutMessage (this.Say "Diagnosing...")
																		(goto state5))))
	(state5 (enter (set-timeout 2))
					(messages (TimeoutMessage (this.Say "Umm... I have no idea.")
																		(start shrug-shoulders-behavior)
																		(start shrug-left-arm-behavior)
																		(start shrug-right-arm-behavior)
																		(goto state6))))
	(state6 (enter (set-timeout 2))
					(messages (TimeoutMessage (this.Say "Sorry, mate, no Internet for you today.")
																		(goto finish))))
	(finish (enter (set-timeout 2))
					(messages (TimeoutMessage (stop shrug-shoulders-behavior)
																		(stop shrug-left-arm-behavior)
																		(stop shrug-right-arm-behavior)
																		(dialog.SetState "stopping")
																		(goto inactive)))))

;; Shutdown
;; --------

(define-state-machine shutdown-state-machine
	(inactive (enter (set-timeout 0)))
	(start (enter (set-timeout 1))
				 (messages (TimeoutMessage (this.Say (String.Format "As you wish, {0}, I am shutting down." username.Value))
																	 (goto state2))))
	(state2 (enter (set-timeout 2))
					(messages (TimeoutMessage (this.Say (String.Format "Actually, I hope you're not in a hurry, {0}..." username.Value))
																		(goto state3))))
	(state3 (enter (set-timeout 2))
					(messages (TimeoutMessage (this.Say "... because I need to install some updates.")
																		(goto state4))))
	(state4 (enter (set-timeout 2))
					(messages (TimeoutMessage (this.Say "This may take a while.")
																		(goto state5))))
	(state5 (enter (set-timeout 2))
					(messages (TimeoutMessage (this.Say "1%...")
																		(goto state6))))
	(state6 (enter (set-timeout 2))
					(messages (TimeoutMessage (this.Say "2%...")
																		(goto state7))))
	(state7 (enter (set-timeout 2))
					(messages (TimeoutMessage (this.Say "3%...")
																		(goto state8))))
	(state8 (enter (set-timeout 2))
					(messages (TimeoutMessage (this.Say "4%...")
																		(goto state9))))
	(state9 (enter (set-timeout 2))
					(messages (TimeoutMessage (this.Say "*YAWN*")
																		(goto state10))))
	(state10 (enter (set-timeout 2))
					 (messages (TimeoutMessage (this.Say "Uhh, where was I?")
																		 (set! doctor.StandUp false)
																		 (goto state11))))
	(state11 (enter (set-timeout 2))
					 (messages (TimeoutMessage (this.Say "6%...")
																		 (goto state12))))
	(state12 (enter (set-timeout 2))
					 (messages (TimeoutMessage (this.Say "7%...")
																		 (set! doctor.SitUp false)
																		 (goto state13))))
	(state13 (enter (set-timeout 2))
					 (messages (TimeoutMessage (this.Say "...")
																		 (goto state14))))
	(state14 (enter (set-timeout 2))
					 (messages (TimeoutMessage (this.Say ".....")
																		 (goto state15))))
	(state15 (enter (set-timeout 2))
					 (messages (TimeoutMessage (this.Say "ZZZZZ........")
																		 (goto state16))))
	(state16 (enter (set-timeout 2))
					 (messages (TextInputMessage
											(this.Say "ZZZZZ.....")
											(goto state16)))))