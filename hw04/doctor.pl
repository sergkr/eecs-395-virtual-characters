%%
%% Rules for matching the user's input
%%     rule(Priority, Pattern, Response, Action)
%% Means when user types input matching Pattern, respond with Response and
%% perform Action.  Action may be any Prolog goal.
%% Conflicts between rules are resolved by priority (1 is highest priority)
%%

rule(1, [phrase(_), shutdown, phrase(_)],
	null,
	shutdown).
rule(1, [exit],
	[exiting],
	faint).
rule(1, [search, phrase(_)],
	[sorry, i, 'can\'t', reach, google, at, the, moment],
	shrug).
rule(1, [dir, phrase(_)],
	[there, are, no, directories],
	do_nothing).
rule(1, [cd, phrase(X)],
	['can\'t', cd, to, directory, X],
	do_nothing).
rule(1, [ping, phrase(X)],
	[X, '\:', pong],
	raise_right_hand).
rule(1, [phrase(_), internet, phrase(_)],
	[accessing, the, internet],
	internet).
rule(1, [word],
	[looks, like, your, license, key, is, invalid],
	do_nothing).
rule(1, [phrase(_), music, phrase(_)],
	[la, la, la, la, la],
	dance).
rule(1, [mkdir, phrase(X)],
	[there, are, no, directories, in, microsoft, fred, '\.', just, say, what, you, want, to, do],
	do_nothing).
rule(1, [chkdsk, phrase(X)],
	[chkdsk, '\:', disk, X, has, errors],
	do_nothing).
rule(1, [copy, word(X), word(Y)],
	['i\'ve', put, some, X, into, Y],
	do_nothing).
rule(1, [defrag, phrase(_)],
	[i, 'won\'t', do, that, right, now, because, it, will, take, forever],
	do_nothing).
rule(1, [echo, phrase(X)],
	[X],
	do_nothing).
rule(1, [phrase(_), help, phrase(_)],
	['let\'s', try, and, figure, it, out, together],
	do_nothing).
% rule(1, [i, am, phrase(X)],
% 	[how, long, have, you, been, X, ?],
% 	do_nothing).
% rule(1, [phrase(_), you, phrase(Verb), me],
% 	[what, makes, you, think, i, Verb, you, ?],
% 	do_nothing).
% rule(1, [phrase(_), you, phrase(X, (X \= []))],
% 	[why, do, you, think, i, X, ?],
% 	do_nothing).
% rule(1, [i, like, phrase(X)],
% 	[does, anyone, else, in, your, family, like, X, ?],
% 	do_nothing).
% rule(1, [i, feel, phrase(_)],
% 	[do, you, often, feel, that, way, ?],
% 	do_nothing).
% rule(1, [i, think, phrase(X)],
% 	[why, do, you, think, X, ?],
% 	do_nothing).
% rule(1, [phrase(_), word(X, important(X)), phrase(_)],
% 	[please, tell, me, more, about, your, X],
% 	do_nothing).
%     important(father).
%     important(mother).
%     important(sister).
%     important(mother).
%     important(brother).
%     important(son).
%     important(daughter).

% Very silly examples of using actions in rules.
% rule(1, [phrase(_), love, you, phrase(_)],
%	[],
%	blush)
%    :- not blushing.
% rule(1, [phrase(_), love, you, phrase(_)],
%	[i, just, remembered, i, have, another, appointment],
%	run_away)
%    :- blushing.
% rule(1, [phrase(_), love, you, phrase(_)],
%	["I, um, ..."],
%	faint)
%    :- blushing.

	
% rule(2, [phrase(_)],
%	[tell, me, about, your, mother],
%	do_nothing).
% rule(2, [phrase(X)],
%	[why, do, you, think, X, ?],
%	do_nothing).
% rule(2, [phrase(_)],
%	[i, think, 'you\'re', afraid, to, tell, me, your, true, feelings],
%	do_nothing).
% rule(2, [phrase(_)],
%	['you\'re', safe, here, (;), you, can, trust, me],
%	do_nothing).
rule(2, [phrase(_)],
	[i, 'can\'t', operate, on, nothing],
	shrug).

blushing :- red(X), property($doctor, "Color", X).
blush :- red(X), set_property($doctor, "Color", X).
faint :- set_property($doctor, "IsAlive", false).
run_away :- lisp([start, 'run-away'], _).
raise_right_hand :- lisp ([start, 'raise-right-hand'], _).
red(Color) :- property($'Color', "Red", Color).
shrug :- lisp([start, 'shrug-left-arm-behavior'], _),
	       lisp([start, 'shrug-right-arm-behavior'], _),
				 lisp([start, 'shrug-shoulders-behavior'], _).
dance :- lisp([start, 'swing-left-right'], _),
	       lisp([start, 'reach-left'], _),
				 lisp([start, 'reach-right'], _),
				 lisp([start, 'bump'], _).
internet :- lisp([[member, 'browse-internet-state-machine', 'SetState'], "start"], _).
shutdown :- lisp([[member, 'shutdown-state-machine', 'SetState'], "start"], _).
